ActiveAdmin.register Negara do
	permit_params :name_negara

	# index do
	# 	selectable_column
	# 	id_column
	# 	column :name_negara
	# 	column :created_at
	# 	actions
	# end

	form do |f|
		f.inputs do
			f.input :name_negara
			f.input :bendera
		end
		f.actions
	end
end
