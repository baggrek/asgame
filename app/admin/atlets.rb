ActiveAdmin.register Atlet do
	permit_params :name_atlet, :foto, :umur, :tinggi,:medal_emas, :medal_perak, :medal_perunggu, :cabor, :negara_id

	# index do
	# 	selectable_column
	# 	id_column
	# 	column :name_atlet	
	# 	column :tinggi
	# 	column :umur
	# 	column :medal_emas
	# 	column :medal_perak
	# 	column :medal_perunggu
	# 	column :cabor_id
	# 	column :negara_id
	# 	column :created_at
	# 	actions 
	# end

	form do |f|
		f.inputs do
			f.input :name_atlet
			f.input :foto
			f.input :umur
			f.input :tinggi
			f.input :medal_emas
			f.input :medal_perak
			f.input :medal_perunggu
			f.input :cabor, :label => 'Cabor', :as => :select, :collection => Cabor.all.map{|c|["#{c.name_cabor}", c.id]}, :prompt => "Choose a Cabor"
			f.input :negara, :label => 'Negara', :as => :select, :collection => Negara.all.map{|c|["#{c.name_negara}", c.id]}, :prompt => "Choose a Negara"
		end
		f.actions
	end
end
