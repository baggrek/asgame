ActiveAdmin.register Cabor do
	permit_params :name_cabor

	# index do
	# 	selectable_column
	# 	id_column
	# 	column :name_cabor
	# 	column :created_at
	# 	actions
	# end

	form do |f|
		f.inputs do
			f.inputs :name_cabor
		end
		f.actions
	end
end
