ActiveAdmin.register Main do
	permit_params :tempat, :time, :atlet_id, :cabor_id

	form do |f|
		f.inputs do
			f.input :tempat
			f.input :time
			f.input :cabor, :label => 'Cabor', :as => :select, :collection => Cabor.all.map{|c|["#{c.name_cabor}", c.id]}, :prompt => "Choose a Cabor"
			# f.input :atlet, :label => 'Atlet', :as => :select, :collection => Negara.all.map{|c|["#{c.name_negara}", c.id]}, :prompt => "Choose a Negara"
		end
		f.actions
	end
end
