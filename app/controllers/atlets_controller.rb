class AtletsController < ApiController
	before_action :set_atlet, only: [:show, :update, :destroy]

	def index
		atlet = Atlet.all
		# response = {status: :OK, result: atlet, errors: nil}
		atlet = atlet.map{|val|
			{
				:id => val.id,
				:name_atlet => val.name_atlet,
				:cabor_id => val.cabor_id
			}}
		render json: atlet

		# barters= barters.map{|val|
		# 		{
		# 			:id => val.id,
		# 			:name => val.product_barter.name, 
		# 			:picture => val.product_barter.picture,
		# 			:user_product => val.product.user_id,
		# 			:user_product_barter => val.product_barter.user_id,
		# 			:status => val.status
		# 		}
		# 	}
		
	end

	def show
		@atlet = Atlet.find(params[:id])
		render json: @atlet
	end

	def create
		atlet = Atlet.create(atlet_params)
		if atlet.save
			response = {status: :OK, result: atlet, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: atlet.errors}
			render json: response
		end
	end

	def update
		if @atlet.update(atlet_params)
			response = {status: :OK, result: @atlet, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @atlet.errors}
			render json: response
		end
		
	end

	def destroy
		if @atlet.destroy
			response = {status: :OK, result: @atlet, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @atlet.errors}
		end
	end

	private

	def set_atlet
		@atlet = Atlet.find(params[:id])
		
	end

	def atlet_params
		permit(:name_atlet, :foto, :umur, :tinggi,:medal_emas, :medal_perak, :medal_perunggu, :cabor_id, :negara_id)
		
	end
end
