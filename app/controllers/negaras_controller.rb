class NegarasController < ApiController
	before_action :set_negara, only: [:show, :update, :destroy]

	def index
		@negara = Negara.all
		# response = {status: :OK, result: @negara, errors: nil}
		render json: @negara
		
	end

	def show
		@negara = Negara.find(params[:id])
		response = {status: :OK, result: @negara, errors: nil}
		render json: response
	end

	def create
		negara = Negara.create(negara_params)
		if negara.save
			response = {status: :OK, result: negara, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: negara.errors}
			render json: response
		end
	end

	def update
		if @negara.update(negara_params)
			response = {status: :OK, result: @negara, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @negara.errors}
			render json: response
		end
		
	end

	def destroy
		if @negara.destroy
			response = {status: :OK, result: @negara, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @negara.errors}
		end
	end

	private

	def set_negara
		@negara = Negara.find(params[:id])
		
	end

	def negara_params
		params.permit(:name_negara, :bendera)
		
	end
end
