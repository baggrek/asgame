class CaborsController < ApiController
	before_action :set_cabor, only: [:show, :update, :destroy]

	def index
		@cabor = Cabor.all
		# response = {status: :OK, result: @cabor, errors: nil}
		render json: @cabor
		
	end

	def show
		@cabor = Cabor.find(params[:id])
		response = {status: :OK, result: @cabor, errors: nil}
		render json: response
	end

	def create
		cabor = Cabor.create(cabor_params)
		if cabor.save
			response = {status: :OK, result: cabor, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: cabor.errors}
			render json: response
		end
	end

	def update
		if @cabor.update(cabor_params)
			response = {status: :OK, result: @cabor, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @cabor.errors}
			render json: response
		end
		
	end

	def destroy
		if @cabor.destroy
			response = {status: :OK, result: @cabor, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @cabor.errors}
		end
	end

	private

	def set_cabor
		@cabor = Cabor.find(params[:id])
		
	end

	def cabor_params
		params.permit(:name_cabor)
		
	end
end
