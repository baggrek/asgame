class MainController < ApiController
	before_action :set_main, only: [:show, :update, :destroy]

	def index
		@main = Main.all
		response = {status: :OK, result: @main, errors: nil}
		render json: response
		
	end

	def show
		@main = Main.find(params[:id])
		response = {status: :OK, result: @main, errors: nil}
		render json: response
	end

	def create
		main = Main.create(main_params)
		if main.save
			response = {status: :OK, result: main, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: main.errors}
			render json: response
		end
	end

	def update
		if @main.update(main_params)
			response = {status: :OK, result: @main, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @main.errors}
			render json: response
		end
		
	end

	def destroy
		if @main.destroy
			response = {status: :OK, result: @main, errors: nil}
			render json: response
		else
			response = {status: :FAIL, result: nil, errors: @main.errors}
		end
	end

	private

	def set_main
		@main = Main.find(params[:id])
		
	end

	def main_params
		params.permit(:tempat, :time, :cabor_id)
		
	end
end
