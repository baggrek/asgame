class Negara < ApplicationRecord
	has_many :atlet

	validates_presence_of :name_negara
	validates :name_negara, uniqueness: {case_sensitive: false}
	mount_uploader :bendera, ImageUploader
end
