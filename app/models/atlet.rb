class Atlet < ApplicationRecord
  belongs_to :cabor
  belongs_to :negara
  has_one :atlet


  validates_presence_of :name_atlet, :umur, :tinggi, :medal_emas, :medal_perak, :medal_perunggu
  validates :name_atlet, uniqueness: {case_sensitive: false}
  mount_uploader :foto, ImageUploader

  def cabor_id
  	self.cabor.name_cabor
  	
  end

  def negara_id
  	self.negara.name_negara
  	
  end
end
