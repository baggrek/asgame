class Cabor < ApplicationRecord
	has_many :atlet
	has_many :main
	
	validates_presence_of :name_cabor
	validates :name_cabor, uniqueness: {case_sensitive: false}
end
