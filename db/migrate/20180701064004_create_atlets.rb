class CreateAtlets < ActiveRecord::Migration[5.2]
  def change
    create_table :atlets do |t|
      t.string :name_atlet
      t.string :foto
      t.integer :umur
      t.integer :tinggi
      t.integer :medal_emas
      t.integer :medal_perak
      t.integer :medal_perunggu
      t.references :cabor, foreign_key: true
      t.references :negara, foreign_key: true

      t.timestamps
    end
  end
end
