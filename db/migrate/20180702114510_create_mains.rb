class CreateMains < ActiveRecord::Migration[5.2]
  def change
    create_table :mains do |t|
      t.string :tempat
      t.time :time
      # t.referencess :atlet, foreign_key: true
      t.references :cabor, foreign_key: true

      t.timestamps
    end
  end
end
