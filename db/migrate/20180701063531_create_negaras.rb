class CreateNegaras < ActiveRecord::Migration[5.2]
  def change
    create_table :negaras do |t|
      t.string :name_negara

      t.timestamps
    end
  end
end
