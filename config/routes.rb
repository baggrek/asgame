Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :negaras
  resources :atlets
  resources :cabors
  resources :mains
end
